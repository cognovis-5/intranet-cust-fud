#

ad_page_contract {
      #etm_customer_marge

    @param start_date Start date (YYYY-MM-DD format) 
    @param end_date End date (YYYY-MM-DD format) 
} {
    { start_date "2015-01-01" }
    { end_date "2016-01-01" }
    { level_of_detail:integer 1 }
    { customer_id:integer 0 }
    { orderby_clause "marge_perc" }
    { rounding_precision:integer 2}
    { number_locale "" }
}


# ------------------------------------------------------------
# Security (New!)
#
# The access permissions for the report are taken from the
# "im_menu" Menu Items in the "Reports" section that link 
# to this report. It's just a convenient way to handle 
# security, that avoids errors (different rights for the 
# Menu Item then for the report) and redundancy.

# What is the "label" of the Menu Item linking to this report?
set menu_label "fud-finance-weeksagkm"

# Get the current user and make sure that he or she is
# logged in. Registration is handeled transparently - 
# the user is redirected to this URL after registration 
# if he wasn't logged in.
set current_user_id [ad_maybe_redirect_for_registration]

# Determine whether the current_user has read permissions. 
# "db_string" takes a name as the first argument 
# ("report_perms") and then executes the SQL statement in 
# the second argument. 
# It returns an error if there is more then one result row.
# im_object_permission_p is a PlPg/SQL procedure that is 
# defined as part of ]project-open[.
set read_p [db_string report_perms "
	select	im_object_permission_p(m.menu_id, :current_user_id, 'read')
	from	im_menus m
	where	m.label = :menu_label
" -default 'f']

# For testing - set manually
set read_p "t"

# Write out an error message if the current user doesn't
# have read permissions and abort the execution of the
# current screen.
if {![string equal "t" $read_p]} {
    set message "You don't have the necessary permissions to view this page"
    ad_return_complaint 1 "<li>$message"
    ad_script_abort
}



# ------------------------------------------------------------
# Check Parameters (New!)
#
# Check that start_date and end_date have correct format.
# We are using a regular expression check here for convenience.

if {![regexp {[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]} $start_date]} {
    ad_return_complaint 1 "Start Date doesn't have the right format.<br>
    Current value: '$start_date'<br>
    Expected format: 'YYYY-MM-DD'"
    ad_script_abort
}

if {![regexp {[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]} $end_date]} {
    ad_return_complaint 1 "End Date doesn't have the right format.<br>
    Current value: '$end_date'<br>
    Expected format: 'YYYY-MM-DD'"
    ad_script_abort
}

# Maxlevel is 3. 
#if {$level_of_detail > 3} { set level_of_detail 3 }
set level_of_detail 1

set locale [lang::user::locale]
if {"" == $number_locale} { set number_locale $locale  }




if {"" == $start_date} { 
    set start_date "$todays_year-01-01"
}

if {"" == $end_date} { 
    set end_date "$todays_year-12-31"
}
# ------------------------------------------------------------
# Page Title, Bread Crums and Help
#
# We always need a "page_title".
# The "context_bar" defines the "bread crums" at the top of the
# page that allow a user to return to the home page and to
# navigate the site.
# Every reports should contain a "help_text" that explains in
# detail what exactly is shown. Reports can get very messy and
# it can become very difficult to interpret the data shown.
#

set page_title "ETM-Customer marge"
set context_bar [im_context_bar $page_title]
#set help_text "<a href=$custanalyst_url>zurueck zu Cust-Analyt</a>"


# ------------------------------------------------------------
# Default Values and Constants
#
# In this section we define constants and default variables
# that are used in the sections further below.
#

# Default report line formatting - alternating between the
# CSS styles "roweven" (grey) and "rowodd" (lighter grey).
#
set rowclass(0) "roweven"
set rowclass(1) "rowodd"

# Variable formatting - Default formatting is quite ugly
# normally. In the future we will include locale specific
# formatting. 
#
set currency_format "999,999,999.09"
set date_format "YYYY-MM-DD"

# Set URLs on how to get to other parts of the system
# for convenience. (New!)
# This_url includes the parameters passed on to this report.
#
set company_url "/intranet/companies/view?company_id="
set project_url "/intranet/projects/view?view_name=finance&project_id="
set invoice_url "/intranet-invoices/view?invoice_id="
set user_url "/intranet/users/view?user_id="
set this_url [export_vars -base "/intranet-reporting-tutorial/projects-05" {start_date end_date} ]
set custanalyst_url [export_vars -base "/intranet-cust-fud/reports/etm-customer_analyt" {start_date end_date} ]

# Level of Details
# Determines the LoD of the grouping to be displayed
#
# set levels {2 "Customers" 3 "Customers+Projects"} 

# help text
set help_text "<a href=$custanalyst_url>zurueck zu Cust-Analyt</a>"

# Sortierung
set orderby { pmlead "CM" project "Projekt" datum "Datum" invoice "Invoice" po "PO" marge "Marge" margeperc "Marge%" } 

if { $orderby == "pmlead" } {
    set orderby_clause "project_lead_id"
    } elseif  { $orderby == "project" } {
    set orderby_clause "project_id"
    } elseif  { $orderby == "datum" } {
    set orderby_clause "start_date"
    } elseif  { $orderby == "invoice" } {
    set orderby_clause "invoice"
    } elseif  { $orderby == "po" } {
    set orderby_clause "cost_purchase_orders_cache"
    } elseif  { $orderby == "marge" } {
    set orderby_clause "marge"
    } elseif  { $orderby == "margeperc" } {
    set orderby_clause "marge_perc"
    }



# ------------------------------------------------------------
# Report SQL - This SQL statement defines the raw data 
# that are to be shown.
#
# This section is usually the starting point when starting 
# any new report.
# Once your SQL is fine you you start adding formatting, 
# grouping and filters to create a "real-world" report.
#

set customer_sql ""
if {0 != $customer_id} {
    set customer_sql "and p.company_id = :customer_id\n"
}

# This version of the select query uses a join with 
# im_companies on (p.company_id = company.company_id).
# This join is possible, because the p.company_id field
# is a non-null field constraint via referential integrity
# to the im_companies table.
# In the absence of such strong integrity contraints you
# will have to use "LEFT OUTER JOIN"s instead. (New!)
#
set report_sql "
select 	  round(p.cost_invoices_cache,2) as invoice
	, round(p.cost_purchase_orders_cache,2) as po
	, round((p.cost_invoices_cache - p.cost_purchase_orders_cache),2) as marge
	, case when p.cost_invoices_cache is null then '0'
	       when p.cost_invoices_cache = 0 then '0'
	       else round(((p.cost_invoices_cache - p.cost_purchase_orders_cache) / p.cost_invoices_cache * 100),0)
	   end as marge_perc
	, case when p.cost_invoices_cache is null or p.cost_invoices_cache = 0 then 1 end as marge_red 
	, case when p.cost_invoices_cache is not null and p.cost_invoices_cache != 0 and p.cost_purchase_orders_cache != 0 and (round(((p.cost_invoices_cache - p.cost_purchase_orders_cache) / p.cost_invoices_cache * 100),0)) > 39 then 1 end as marge_green 
	, case when p.cost_invoices_cache is not null and p.cost_invoices_cache != 0 and (round(((p.cost_invoices_cache - p.cost_purchase_orders_cache) / p.cost_invoices_cache * 100),0))  < 20 then 1 end as marge_red 
	, case when p.cost_invoices_cache is not null and p.cost_invoices_cache != 0 and (round(((p.cost_invoices_cache - p.cost_purchase_orders_cache) / p.cost_invoices_cache * 100),0))  between 19 and 39 then 1 end as marge_orange 
	, case when p.cost_invoices_cache is null then p.cost_purchase_orders_cache
	       when p.cost_invoices_cache = 0 then p.cost_purchase_orders_cache
	       else '0'
	   end as probe_cost
	, im_name_from_user_id(p.project_lead_id) as project_lead_name
	, to_char(p.start_date, 'YYYY-MM-DD') as start_date_formatted
	, im_name_from_id(company_id) as customer
,*


from im_projects p
where 	parent_id is null
	and p.start_date >= :start_date
	and p.end_date <= :end_date
	and p.project_nr not like '1%'
	and p.project_nr not like 'Q%'
	and p.project_nr not like 'q%'
	and p.project_nr not like 'z%'
order by marge_perc --$orderby_clause
		
"



# ------------------------------------------------------------
# Report Definition
#

# Global Header Line
# set header0 {
# 	"Project<br>Manager" 
# 	"Project<br>Name" 
# 	"Kunde"
# 	"Datum"
# 	"Invoices"
# 	"POs"
# 	"KostenProbe"
# 	"Marge"
# 	"Marge %"
# 	">40% (Gruen)"
# 	"21-49% (Orange)"
# 	"<20% (Rot)"
# }

set header0 {
	"Project<br>Manager" 
	"Project<br>Name" 
	"Kunde"
	"Datum"
	"Invoices"
	"POs"
	"KostenProbe"
	"Marge"
	"Marge %"

}

# The entries in this list include <a HREF=...> tags
# in order to link the entries to the rest of the system (New!)
#
set report_def [list \
    group_by project_id \
    header { 
	    $project_lead_name
	    "<a href=$project_url$project_id>$project_name</a"
	    $customer
	    $start_date_formatted
	    $invoice
	    $po
	    $probe_cost
	    $marge
	    "$marge_perc %"

	    } \
	    content {} \
            footer {} \
]


set ttt {
		"<nobr><i>$po_per_invoice_perc</i></nobr>"
		"<nobr><i>$gross_profit</i></nobr>"
}


# Global Footer Line
 set footer0 {}
set footer0 {
	"" 
	"" 
        "<br><b>Total:</b>"
	"<br><b>$invoice_total</b>"
	"<br><b>$po_total</b>"
	"<br><b>$probe_cost_total</b>"
	"<br><b>$marge_total</b>"
	""
	"<br><font color=green><nobr>Total >40% : <b>$marge_green_total</b></nobr></font><br>
	<nobr><font color=orange>Total 21-39% : <b>$marge_orange_total</b></font></b></nobr><br>
	<nobr><font color=red>Total <20% : <b>$marge_red_total</b></font></nobr>"

}


# ------------------------------------------------------------
# Counters (New!)
#


#
# Subtotal Counters (per project)
#
set invoice_subtotal_counter [list \
        pretty_name "Invoice Amount" \
        var invoice_subtotal \
        reset \$project_id \
        expr "\$cost_invoices_cache+0" \
]



set po_subtotal_counter [list \
        pretty_name "Po Amount" \
        var po_subtotal \
        reset \$project_id \
        expr "\$cost_purchase_orders_cache+0" \
]

set probe_cost_subtotal_counter [list \
        pretty_name "PO test Amount" \
        var probe_cost_subtotal \
        reset \$project_id \
        expr "\$probe_cost+0" \
]

set marge_subtotal_counter [list \
        pretty_name "Marge Amount" \
        var marge_subtotal \
        reset \$project_id \
        expr "\$marge+0" \
]

#
# Grand Total Counters
#
set invoice_grand_total_counter [list \
        pretty_name "Invoice Amount" \
        var invoice_total \
        reset 0 \
        expr "\$cost_invoices_cache+0" \
]


set po_grand_total_counter [list \
        pretty_name "Po Amount" \
        var po_total \
        reset 0 \
        expr "\$cost_purchase_orders_cache+0" \
]

set probe_cost_grand_total_counter [list \
        pretty_name "Probe Amount" \
        var probe_cost_total \
        reset 0 \
        expr "\$probe_cost+0" \
]


set marge_grand_total_counter [list \
        pretty_name "marge Amount" \
        var marge_total \
        reset 0 \
        expr "\$marge+0" \
]

set marge_green_grand_total_counter [list \
        pretty_name "marge green Amount" \
        var marge_green_total \
        reset 0 \
        expr "\$marge_green+0" \
]
set marge_orange_grand_total_counter [list \
        pretty_name "marge orange Amount" \
        var marge_orange_total \
        reset 0 \
        expr "\$marge_orange+0" \
]
set marge_red_grand_total_counter [list \
        pretty_name "marge red Amount" \
        var marge_red_total \
        reset 0 \
        expr "\$marge_red+0" \
]


set counters [list \
	$invoice_subtotal_counter \
	$po_subtotal_counter \
	$probe_cost_subtotal_counter \
	$marge_subtotal_counter \
	$invoice_grand_total_counter \
	$po_grand_total_counter \
	$probe_cost_grand_total_counter \
	$marge_grand_total_counter \
	$marge_green_grand_total_counter \
	$marge_orange_grand_total_counter \
	$marge_red_grand_total_counter \
]

# Set the values to 0 as default (New!)
set invoice_total 0
set po_total 0
set probe_cost_total 0
set marge_total 0


# ------------------------------------------------------------
# Start Formatting the HTML Page Contents
#
# Writing out a report can take minutes and hours, so we are
# writing out the HTML contents incrementally to the HTTP 
# connection, allowing the user to read the first lines of the
# report (in particular the help_text) while the rest of the
# report is still being calculated.
#

# Write out the report header with Parameters
# We use simple "input" type of fields for start_date 
# and end_date with default values coming from the input 
# parameters (the "value='...' section).
#

#
ad_return_top_of_page "
	[im_header]
	[im_navbar]
	<table cellspacing=0 cellpadding=0 border=0>
	<tr valign=top>
	  <td width='30%'>
		<!-- 'Filters' - Show the Report parameters -->
		<form>
		<table cellspacing=2>
		<tr class=rowtitle>
		  <td class=rowtitle colspan=2 align=center>Filters</td>
		</tr>
		  <td><nobr>Start Date:</nobr></td>
		  <td><input type=text name=start_date value='$start_date'></td>
		</tr>
		<tr>
		  <td>End Date:</td>
		  <td><input type=text name=end_date value='$end_date'></td>
		</tr>
		<tr>
		  <td class=form-label>Sortieren nach</td>
		  <td class=form-widget>
		    [im_select -translate_p 0 orderby $orderby $orderby]
		  </td>
		</tr>
		<tr>
		  <td</td>
		  <td><input type=submit value='Submit'></td>
		</tr>
		</table>
		</form>
	  </td>
	  <td align=center>
		<table cellspacing=2 width='90%'>
		<tr>
		  <td>$help_text</td>
		</tr>
		</table>
	  </td>
	</tr>
	</table>
	
	<!-- Here starts the main report table -->
	<table border=0 cellspacing=1 cellpadding=1>
"

# The following report loop is "magic", that means that 
# you don't have to fully understand what it does.
# It loops through all the lines of the SQL statement,
# calls the Report Engine library routines and formats 
# the report according to the report_def definition.

set footer_array_list [list]
set last_value_list [list]

im_report_render_row \
    -row $header0 \
    -row_class "rowtitle" \
    -cell_class "rowtitle"

set counter 0
set class ""
db_foreach sql $report_sql {

	# Select either "roweven" or "rowodd" from
	# a "hash", depending on the value of "counter".
	# You need explicite evaluation ("expre") in TCL
	# to calculate arithmetic expressions. 
	set class $rowclass([expr $counter % 2])

	# Restrict the length of the project_name to max.
	# 40 characters. (New!)
	set project_name [string_truncate -len 40 $project_name]

	im_report_display_footer \
	    -group_def $report_def \
	    -footer_array_list $footer_array_list \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
	# Update counters
	im_report_update_counters -counters $counters

	# Calculated Variables
# 	set po_per_invoice_perc "undef"
# 	if {[expr $invoice_subtotal+0] != 0} {
# 	  set po_per_invoice_perc [expr int(10000.0 * $po_subtotal / $invoice_subtotal) / 100.0]
# 	  set po_per_invoice_perc "$po_per_invoice_perc %"
# 	}
# 	set gross_profit [expr $invoice_subtotal - $po_subtotal]
# 	
# 	set po_per_invoice_perc [expr int(10000.0 * $po_subtotal / $invoice_subtotal) / 100.0]
# 	set po_per_invoice_perc_pretty [lc_numeric $po_per_invoice_perc %.${rounding_precision}f $number_locale]
# 	set po_per_invoice_perc "$po_per_invoice_perc_pretty %"
# 	

	  if { $marge_perc < 20 || [expr $cost_purchase_orders_cache+0] == 0 } { 
	    set marge_perc  "<font color=red>$marge_perc</font>"
	    set marge  "<font color=red>$marge</font>"
	    }  elseif { $marge_perc > 39 && [expr $cost_purchase_orders_cache+0] != 0 } { 
	        set marge_perc  "<font color=green>$marge_perc</font>"
		set marge  "<font color=green>$marge</font>"
	    } elseif { $marge_perc > 19 &&  $marge_perc < 40 } { 
	        set marge_perc  "<font color=orange>$marge_perc</font>"
		set marge  "<font color=orange>$marge</font>"
	    } 
# formatting numbers
	set invoice_subtotal_pretty [lc_numeric $invoice_subtotal %.${rounding_precision}f $number_locale]
	set po_subtotal_pretty [lc_numeric $po_subtotal %.${rounding_precision}f $number_locale]
	set probe_cost_subtotal_pretty [lc_numeric $probe_cost_subtotal %.${rounding_precision}f $number_locale]
	set marge_subtotal_pretty [lc_numeric $marge_subtotal %.${rounding_precision}f $number_locale]
	
	set invoice_total_pretty [lc_numeric $invoice_total %.${rounding_precision}f $number_locale]
	set po_total_pretty [lc_numeric $po_total %.${rounding_precision}f $number_locale]
	set probe_cost_total_pretty [lc_numeric $probe_cost_total %.${rounding_precision}f $number_locale]
	set marge_total_pretty [lc_numeric $marge_total %.${rounding_precision}f $number_locale]	
	
	
	
	set last_value_list [im_report_render_header \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
	]

	set footer_array_list [im_report_render_footer \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
	]

	incr counter
}

im_report_display_footer \
    -group_def $report_def \
    -footer_array_list $footer_array_list \
    -last_value_array_list $last_value_list \
    -level_of_detail $level_of_detail \
    -display_all_footers_p 1 \
    -row_class $class \
    -cell_class $class

im_report_render_row \
    -row $footer0 \
    -row_class $class \
    -cell_class $class \
    -upvar_level 1


# Write out the HTMl to close the main report table
# and write out the page footer.
#
ns_write "
	</table>
	[im_footer]
"

