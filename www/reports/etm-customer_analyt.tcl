# /packages/intranet-reporting-finance/www/finance-income-statement.tcl
#
# Copyright (C) 2003 - 2009 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/ for licensing details.


ad_page_contract {
	testing reports	
    @param start_year Year to start the report
    @param start_unit Month or week to start within the start_year
} {
	{ timerangeset "WEEK" } 
	{ timerangeopt "week" }
	{ start_date "" }
	{ end_date "" }
	{ level_of_detail 3 }
	{ output_format "html" }
	{ number_locale "" }
	{ customer_id:integer 0}
	{ rounding_precision:integer 2}									
}

# ------------------------------------------------------------
# Security

# Label: Provides the security context for this report
# because it identifies unquely the report's Menu and
# its permissions.
set current_user_id [ad_maybe_redirect_for_registration]
#set menu_label "etm-customer-analyst"
set menu_label "fud-finance-weeksagkm"

set read_p [db_string report_perms "
	select	im_object_permission_p(m.menu_id, :current_user_id, 'read')
	from	im_menus m
	where	m.label = :menu_label
" -default 'f']

if {![string equal "t" $read_p]} {
    ad_return_complaint 1 "<li>
[lang::message::lookup "" intranet-reporting.You_dont_have_permissions "You don't have the necessary permissions to view this page"]"
    return
}


# ------------------------------------------------------------
# Defaults

set rowclass(0) "roweven"
set rowclass(1) "rowodd"

set days_in_past 0

set default_currency [ad_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]
set cur_format [im_l10n_sql_currency_format]
set date_format "YYYY-MM-DD"

db_1row todays_date "
select
	to_char(sysdate::date - :days_in_past::integer, 'YYYY') as todays_year,
	to_char(sysdate::date - :days_in_past::integer, 'MM') as todays_month,
	to_char(sysdate::date - :days_in_past::integer, 'DD') as todays_day
from dual
"

if {"" == $start_date} { 
    set start_date "$todays_year-01-01"
}

if {"" == $end_date} { 
    set end_date "$todays_year-12-31"
}

#if {"" == $timerangeopt} {
#     set timerangeopt 'week'
#}

# Maxlevel is 4. Normalize in order to show the right drop-down element
if {$level_of_detail > 4} { set level_of_detail 4 }


db_1row end_date "
select
	to_char(to_date(:start_date, 'YYYY-MM-DD') + 31::integer, 'YYYY') as end_year,
	to_char(to_date(:start_date, 'YYYY-MM-DD') + 31::integer, 'MM') as end_month,
	to_char(to_date(:start_date, 'YYYY-MM-DD') + 31::integer, 'DD') as end_day
from dual
"

db_1row todays_week "
select
	
	(SELECT EXTRACT (WEEK FROM sysdate::date)) AS todays_week
from dual
"


switch $timerangeopt {
	1 { set timerangeset "WEEK" }
	2 { set timerangeset "MONTH" }
	3 { set timerangeset "YEAR" }
}

set company_url "/intranet/companies/view?company_id="
set project_url "/intranet/projects/view?project_id="
set project_fin_url "/intranet/projects/view?view_name=finance&project_id="
set invoice_url "/intranet-invoices/view?invoice_id="
set user_url "/intranet/users/view?user_id="
set this_url [export_vars -base "/intranet-cust-fud/reports/etm-customer_analyt" {start_date end_date} ]
#set custommarge_url [export_vars -base "/intranet-cust-fud/reports/etm-customer_marge" {start_date end_date} ]
set custommarge_url [export_vars -base "/intranet-cust-fud/reports/etm-customer_marge" {start_date end_date} ]

set cm_month "/intranet-cust-fud/reports/etm-finance-month_CM-All"


# ------------------------------------------------------------
# Constants
#

set start_years {2000 2000 2001 2001 2002 2002 2003 2003 2004 2004 2005 2005 2006 2006 2007}
set start_months {01 Jan 02 Feb 03 Mar 04 Apr 05 May 06 Jun 07 Jul 08 Aug 09 Sep 10 Oct 11 Nov 12 Dec}
set start_weeks {01 1 02 2 03 3 04 4 05 5 06 6 07 7 08 8 09 9 10 10 11 11 12 12 13 13 14 14 15 15 16 16 17 17 18 18 19 19 20 20 21 21 22 22 23 23 24 24 25 25 26 26 27 27 28 28 29 29 30 30 31 31 32 32 33 33 34 34 35 35 36 36 37 37 38 38 39 39 40 40 41 41 42 42 43 43 44 44 45 45 46 46 47 47 48 48 49 49 50 50 51 51 52 52}
set start_days {01 1 02 2 03 3 04 4 05 5 06 6 07 7 08 8 09 9 10 10 11 11 12 12 13 13 14 14 15 15 16 16 17 17 18 18 19 19 20 20 21 21 22 22 23 23 24 24 25 25 26 26 27 27 28 28 29 29 30 30 31 31}
set levels {1 "Zeit" 2 "Zeit+Agentur" 3 "Zeit+Agentur+KM" 4 "Zeit+Ag+KM+Proj"} 
set timerangeopt {week "Woche" month "Monat" year "Jahr"}


# ------------------------------------------------------------
# Argument Checking

# Check that Start & End-Date have correct format
if {"" != $start_date && ![regexp {^[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]$} $start_date]} {
    ad_return_complaint 1 "Start Date doesn't have the right format.<br>
    Current value: '$start_date'<br>
    Expected format: 'YYYY-MM-DD'"
}

#if {"" != $end_date && ![regexp {^[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]$} $end_date]} {
#    ad_return_complaint 1 "End Date doesn't have the right format.<br>
#    Current value: '$end_date'<br>
#    Expected format: 'YYYY-MM-DD'"
#}


# ------------------------------------------------------------
# Page Settings

set page_title "Customer Analyst"
set context_bar [im_context_bar $page_title]
set context ""

set help_text "
<strong>Profits</strong><br>

<a href=$custommarge_url><nobr>--> Auftrags Marge</nobr></a>
<a href=$cm_month><nobr>-->CM-Umsatz</nobr></a>
"

# ------------------------------------------------------------
# Set the default start and end date

# ------------------------------------------------------------
# Defaults

set rowclass(0) "roweven"
set rowclass(1) "rowodd"

set days_in_past 0

set default_currency [ad_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]
set cur_format [im_l10n_sql_currency_format]
set date_format "YYYY-MM-DD"

db_1row todays_date "
select
	to_char(sysdate::date - :days_in_past::integer, 'YYYY') as todays_year,
	to_char(sysdate::date - :days_in_past::integer, 'MM') as todays_month,
	to_char(sysdate::date - :days_in_past::integer, 'DD') as todays_day
from dual
"

if {"" == $start_date} { 
    set start_date "$todays_year-01-01"
}

if {"" == $end_date} { 
    set end_date '2099-12-31'
}

#if {"" == $timerangeopt} {
#     set timerangeopt 'week'
#}

# Maxlevel is 4. Normalize in order to show the right drop-down element
if {$level_of_detail > 4} { set level_of_detail 4 }


db_1row end_date "
select
	to_char(to_date(:start_date, 'YYYY-MM-DD') + 31::integer, 'YYYY') as end_year,
	to_char(to_date(:start_date, 'YYYY-MM-DD') + 31::integer, 'MM') as end_month,
	to_char(to_date(:start_date, 'YYYY-MM-DD') + 31::integer, 'DD') as end_day
from dual
"

db_1row todays_week "
select
	
	(SELECT EXTRACT (WEEK FROM sysdate::date)) AS todays_week
from dual
"


switch $timerangeopt {
	1 { set timerangeset "WEEK" }
	2 { set timerangeset "MONTH" }
	3 { set timerangeset "YEAR" }
}


# ------------------------------------------------------------
# Conditional SQL Where-Clause
#

set criteria [list]

if {0 != $customer_id} {
    lappend criteria "cust.company_id = :customer_id"
}

set where_clause [join $criteria " and\n            "]
if { ![empty_string_p $where_clause] } {
    set where_clause " and $where_clause"
}



# ------------------------------------------------------------
# Define the report - SQL, counters, headers and footers 
#

set sql "
select *
from (
 (select 
	'1' as project_count,
	'0' as quote_count,
	'0' as q2o_count,
	p.project_id,
	p.project_name,
	'' as quote_name,
	p.cost_invoices_cache,
	'0.00' as cost_quotes_cache,
	p.start_date,
	to_char(p.start_date,'YYYY-MM-DD') as start_date_formatted,
	im_name_from_id(p.project_lead_id),
	p.project_lead_id,
	p.project_lead_id as project_manager_id,
	im_name_from_user_id(p.project_lead_id) as project_manager_name,
	im_name_from_id(p.interco_company_id) as agency,
	p.interco_company_id,
	p.project_path,
	p.quote_date,
	to_char(p.start_date, 'YYYY') as year,	
	to_char(p.start_date, 'Q') as quater,
	to_char(p.start_date, 'YYYY-Q') as year_quater,
	(SELECT EXTRACT (WEEK FROM p.start_date)) AS week,
	(SELECT EXTRACT (MONTH FROM p.start_date)) AS month,
	to_char(p.start_date,'YYYY-MM-DD') as project_date,
	(SELECT EXTRACT ($timerangeset FROM p.start_date)) as timerange
from	im_projects p
where	p.project_status_id not in ('82','83')
	and p.project_name not like '1%'
	and p.project_name not like '2%'
) 

UNION ALL
(select 
	'0' as project_count,
	'1' as quote_count,
	case when p.project_name not like '1%' then'1' 
	     when p.project_name not like '2%' then'1' 
	else '0' end as q2o_count,
	p.project_id,
	p.project_name,
	case 	when p.project_name like '1%' then '' 
                when p.project_name like '2%' then '' 
		when p.quote_nr is null and p.project_path is not null then p.project_path 
		else p.quote_nr end as quote_name,
	'0.00' as cost_invoices_cache,
	p.cost_quotes_cache,
	p.start_date,
	to_char(p.start_date,'YYYY-MM-DD') as start_date_formatted,
	im_name_from_id(p.project_lead_id),
	p.project_lead_id,
	p.project_lead_id as project_manager_id,
	im_name_from_user_id(p.project_lead_id) as project_manager_name,
	im_name_from_id(p.interco_company_id) as agency,
	p.interco_company_id,
	p.project_path,
	p.quote_date,
	case 	when p.quote_date is null and p.project_path like '1%' then to_char(to_date(substring(p.project_path from 1 for 6),'YYMMDD'),'YYYY') 
		when p.quote_date is null and p.project_path not like '1%' then to_char(p.start_date, 'YYYY')
		when p.quote_date is null and p.project_path like '2%' then to_char(to_date(substring(p.project_path from 1 for 6),'YYMMDD'),'YYYY') 
		when p.quote_date is null and p.project_path not like '2%' then to_char(p.start_date, 'YYYY')
		else to_char(p.quote_date, 'YYYY')
		end as year,
	case 	when p.quote_date is null and p.project_path like '1%' then to_char(to_date(substring(p.project_path from 1 for 6),'YYMMDD'),'Q') 
		when p.quote_date is null and p.project_path not like '1%' then to_char(p.start_date, 'Q')
		when p.quote_date is null and p.project_path like '2%' then to_char(to_date(substring(p.project_path from 1 for 6),'YYMMDD'),'Q') 
		when p.quote_date is null and p.project_path not like '2%' then to_char(p.start_date, 'Q')
		else to_char(p.quote_date, 'Q')
		end as quater,
	case 	when p.quote_date is null and p.project_path like '1%' then to_char(to_date(substring(p.project_path from 1 for 6),'YYMMDD'),'YYYY-Q') 
		when p.quote_date is null and p.project_path not like '1%' then to_char(p.start_date, 'YYYY-Q')
		when p.quote_date is null and p.project_path like '2%' then to_char(to_date(substring(p.project_path from 1 for 6),'YYMMDD'),'YYYY-Q') 
		when p.quote_date is null and p.project_path not like '2%' then to_char(p.start_date, 'YYYY-Q')
		else to_char(p.quote_date, 'YYYY-Q')
		end as year_quarter,
	case 	when p.quote_date is null and p.project_path like '1%' then (SELECT EXTRACT (WEEK FROM (to_date(substring(p.project_path from 1 for 6),'YYMMDD')))) 
		when p.quote_date is null and p.project_path not like '1%' then (SELECT EXTRACT (WEEK FROM p.start_date))
		when p.quote_date is null and p.project_path like '2%' then (SELECT EXTRACT (WEEK FROM (to_date(substring(p.project_path from 1 for 6),'YYMMDD')))) 
		when p.quote_date is null and p.project_path not like '2%' then (SELECT EXTRACT (WEEK FROM p.start_date))
		else (SELECT EXTRACT (WEEK FROM p.quote_date)) 
		end as week,
	case 	when p.quote_date is null and p.project_path like '1%' then (SELECT EXTRACT (MONTH FROM (to_date(substring(p.project_path from 1 for 6),'YYMMDD'))))
		when p.quote_date is null and p.project_path not like '1%' then (SELECT EXTRACT (MONTH FROM p.start_date))
		when p.quote_date is null and p.project_path like '2%' then (SELECT EXTRACT (MONTH FROM (to_date(substring(p.project_path from 1 for 6),'YYMMDD'))))
		when p.quote_date is null and p.project_path not like '2%' then (SELECT EXTRACT (MONTH FROM p.start_date))
		else (SELECT EXTRACT (MONTH FROM p.quote_date)) 
		end as month,
	case 	when p.quote_date is null and p.project_path like '1%' then to_char(to_date(substring(p.project_path from 1 for 6),'YYMMDD'),'YYYY-MM-DD') 
		when p.quote_date is null and p.project_path not like '1%' then to_char(p.start_date, 'YYYY-MM-DD')
                when p.quote_date is null and p.project_path like '2%' then to_char(to_date(substring(p.project_path from 1 for 6),'YYMMDD'),'YYYY-MM-DD') 
		when p.quote_date is null and p.project_path not like '2%' then to_char(p.start_date, 'YYYY-MM-DD')
		else to_char(p.quote_date, 'YYYY-MM-DD')
		end as project_date,
	case 	when p.quote_date is null and p.project_path like '1%' then (SELECT EXTRACT ($timerangeset FROM (to_date(substring(p.project_path from 1 for 6),'YYMMDD')))) 
		when p.quote_date is null and p.project_path not like '1%' then (SELECT EXTRACT ($timerangeset FROM p.start_date))
		when p.quote_date is null and p.project_path like '2%' then (SELECT EXTRACT ($timerangeset FROM (to_date(substring(p.project_path from 1 for 6),'YYMMDD')))) 
		when p.quote_date is null and p.project_path not like '2%' then (SELECT EXTRACT ($timerangeset FROM p.start_date))
		else (SELECT EXTRACT ($timerangeset FROM p.quote_date)) 
		end as timerange
from	im_projects p
where	p.project_status_id not in ('82','83')
)
) proj
where proj.project_date > :start_date
      and proj.project_date <= :end_date
      and proj.project_name not like 'SK%'
order by
	--(SELECT EXTRACT (WEEK FROM p.start_date)) DESC,	
	timerange DESC,	
	im_name_from_id(proj.interco_company_id),
	im_name_from_user_id(proj.project_lead_id),
	cost_invoices_cache DESC,
	proj.project_name DESC
"


# Report
set report_def [list \
    group_by timerange \
    header {
	<b>$year</b>
	<b>$timerangeset $timerange</b>
    } \
        content [list \
            group_by interco_company_id \
            header { } \
	    content [list \
	           group_by project_manager_name \
	            header { } \
		    content [list \
			    header {
				""
				""
				""
				""
				"<a href=$project_fin_url$project_id>$project_name</a>"
				"<i>$cost_invoices_cache_pretty $default_currency</i>" 
				"<i>$cost_quotes_cache_pretty $default_currency</i>" 
				""
				""
				$q2o_count
			    } \
			    content {} \
		    ] \
	            footer {
			""
			""
			""
			$project_manager_name
			""
			"<i>$invoice_pm_subtotal $default_currency</i>" 
			"<i>$quote_pm_subtotal $default_currency</i>" 
			"<i>$project_count_pm_subtotal</i>" 
			"<i>$quote_count_pm_subtotal</i>" 
			"<i>$q2o_count_pm_subtotal</i>" 
	            } \
	    ] \
	footer {
		"" 
		"" 
		<b>$agency</b>
		"" 
		""
		"<i><b>$invoice_ag_subtotal $default_currency</b></i>" 
		"<i><b>$quote_ag_subtotal $default_currency</b></i>" 
		"<i><b>$project_count_ag_subtotal</b></i>" 
		"<i><b>$quote_count_ag_subtotal</b></i>" 
		"<i><b>$q2o_count_ag_subtotal</b></i>" 
	} \
    ] \
    footer {  
		"" 
		""
		"" 
		"<b>Total $timerangeset $timerange</b>" 
		""
		"<nobr><b>$invoice_subtotal $default_currency</b></nobr>" 
		"<nobr><b>$quote_subtotal $default_currency</b></nobr>"
		"<nobr><b>$project_count_subtotal</b></nobr>"
		"<nobr><b>$quote_count_subtotal</b></nobr>"
		"<nobr><b>$q2o_count_subtotal</b></nobr>"
		
    } \
]


set invoice_total 0
set quote_total 0
set project_count_total 0
set quote_count_total 0
set q2o_count_total 0



# Global header/footer
set header0 {"Jahr" "Zeit" "Agentur" "KM" "____" "Auftrag" "KVA" "Auftragsanzahl" "KVA-Anzahl" "KVA beauftrag?"}
set footer0 {
	""	
	"" 
	"" 
	""
	"<br><b>Total:</b>" 
	"<br><b>$invoice_total $default_currency</b>" 
	"<br><b>$quote_total $default_currency</b>" 
	"<br><b>$project_count_total</b>" 
	"<br><b>$quote_count_total</b>" 
	"<br><b>$q2o_count_total</b>" 
	
}

set invoice_total 0
set quote_total 0
set project_count_total 0
set quote_count_total 0
set q2o_count_total 0

#
# Subtotal Counters (per project)
#
set invoice_subtotal_counter [list \
        pretty_name "Invoice Amount" \
        var invoice_subtotal \
        reset \$timerange \
        expr "\$cost_invoices_cache+0" \
]

set quote_subtotal_counter [list \
        pretty_name "Quote Amount" \
        var quote_subtotal \
        reset \$timerange \
        expr "\$cost_quotes_cache+0" \
]

set project_count_subtotal_counter [list \
        pretty_name "Project Count" \
        var project_count_subtotal \
        reset \$timerange \
        expr "\$project_count+0" \
]

set quote_count_subtotal_counter [list \
        pretty_name "Project Count" \
        var quote_count_subtotal \
        reset \$timerange \
        expr "\$quote_count+0" \
]
set q2o_count_subtotal_counter [list \
        pretty_name "q2o Count" \
        var q2o_count_subtotal \
        reset \$timerange \
        expr "\$q2o_count+0" \
]

#
# Subtotal Counters (per agency)
#
set invoice_ag_subtotal_counter [list \
        pretty_name "Invoice Amount" \
        var invoice_ag_subtotal \
        reset \$interco_company_id \
        expr "\$cost_invoices_cache+0" \
]

set quote_ag_subtotal_counter [list \
        pretty_name "Quote Amount" \
        var quote_ag_subtotal \
        reset \$interco_company_id \
        expr "\$cost_quotes_cache+0" \
]

set project_count_ag_subtotal_counter [list \
        pretty_name "Project Count" \
        var project_count_ag_subtotal \
        reset \$interco_company_id \
        expr "\$project_count+0" \
]

set quote_count_ag_subtotal_counter [list \
        pretty_name "Project Count" \
        var quote_count_ag_subtotal \
        reset \$interco_company_id \
        expr "\$quote_count+0" \
]
set q2o_count_ag_subtotal_counter [list \
        pretty_name "q2o Count" \
        var q2o_count_ag_subtotal \
        reset \$interco_company_id \
        expr "\$q2o_count+0" \
]
#
# PM Counters (per project)
#
set invoice_pm_counter [list \
        pretty_name "Invoice Amount" \
        var invoice_pm_subtotal \
        reset \$project_manager_id \
        expr "\$cost_invoices_cache+0" \
]

set quote_pm_counter [list \
        pretty_name "Quote Amount" \
        var quote_pm_subtotal \
        reset \$project_manager_id \
        expr "\$cost_quotes_cache+0" \
]

set project_count_pm_counter [list \
        pretty_name "Project Count" \
        var project_count_pm_subtotal \
        reset \$project_manager_id \
        expr "\$project_count+0" \
]

set quote_count_pm_counter [list \
        pretty_name "Project Count" \
        var quote_count_pm_subtotal \
        reset \$project_manager_id \
        expr "\$quote_count+0" \
]
set q2o_count_pm_counter [list \
        pretty_name "Project Count" \
        var q2o_count_pm_subtotal \
        reset \$project_manager_id \
        expr "\$q2o_count+0" \
]

#
# Grand Total Counters
#
set invoice_grand_total_counter [list \
        pretty_name "Invoice Amount" \
        var invoice_total \
        reset 0 \
        expr "\$cost_invoices_cache+0" \
]

set quote_grand_total_counter [list \
        pretty_name "Quote Amount" \
        var quote_total \
        reset 0 \
        expr "\$cost_quotes_cache+0" \
]

set project_count_grand_total_counter [list \
        pretty_name "Project Count" \
        var project_count_total \
        reset 0 \
        expr "\$project_count+0" \
]

set quote_count_grand_total_counter [list \
        pretty_name "Project Count" \
        var quote_count_total \
        reset 0 \
        expr "\$quote_count+0" \
]
set q2o_count_grand_total_counter [list \
        pretty_name "Project Count" \
        var q2o_count_total \
        reset 0 \
        expr "\$q2o_count+0" \
]



set counters [list \
	$invoice_subtotal_counter \
	$quote_subtotal_counter \
	$project_count_subtotal_counter \
	$quote_count_subtotal_counter \
	$q2o_count_subtotal_counter \
	$invoice_ag_subtotal_counter \
	$quote_ag_subtotal_counter \
	$project_count_ag_subtotal_counter \
	$quote_count_ag_subtotal_counter \
	$q2o_count_ag_subtotal_counter \
	$invoice_pm_counter \
	$quote_pm_counter \
	$project_count_pm_counter \
	$quote_count_pm_counter \
	$q2o_count_pm_counter \
	$invoice_grand_total_counter \
	$quote_grand_total_counter \
	$project_count_grand_total_counter \
	$quote_count_grand_total_counter \
	$q2o_count_grand_total_counter \	
]



# ------------------------------------------------------------
# Start formatting the page header
#

# Write out HTTP header, considering CSV/MS-Excel formatting
im_report_write_http_headers -output_format $output_format

switch $output_format {
    html {
	ns_write "
	[im_header]
	[im_navbar]
	<table cellspacing=0 cellpadding=0 border=0>
	<tr valign=top>
	<td>
	<form>
                [export_form_vars project_id project_manager_id]
		<table border=0 cellspacing=1 cellpadding=1>
		<tr>
		  <td class=form-label>Zeiteintlg</td>
		  <td class=form-widget>
		     [im_select -translate_p 1 timerangeset $timerangeopt $timerangeset]
		  </td>
		</tr>

		<tr>
		  <td class=form-label>Level of Details</td>
		  <td class=form-widget>
		    [im_select -translate_p 0 level_of_detail $levels $level_of_detail]
		  </td>
		</tr>
	
		<tr>
		  <td class=form-label>Start Date</td>
		  <td class=form-widget>
		    <input type=textfield name=start_date value=$start_date>
		  </td>
		</tr>
		<tr>
		  <td class=form-label>End Date</td>
		  <td class=form-widget>
		    <input type=textfield name=end_date value=$end_date>
		  </td>
		</tr>
                <tr>
                  <td class=form-label>Format</td>
                  <td class=form-widget>
                    [im_report_output_format_select output_format $output_format]
                  </td>
                </tr>
		<tr>
		  <td class=form-label></td>
		  <td class=form-widget><input type=submit value=Submit></td>
		</tr>
		</table>
	</form>
	</td>
	<td>
		<table cellspacing=2 width=90%>
		<tr><td>$help_text</td></tr>
		</table>
	</td>
	</tr>
	</table>
	<table border=0 cellspacing=1 cellpadding=1>\n"
    }
}	


# ------------------------------------------------------------
# Start formatting the report body
#

im_report_render_row \
    -output_format $output_format \
    -row $header0 \
    -row_class "rowtitle" \
    -cell_class "rowtitle"


set footer_array_list [list]
set last_value_list [list]
set class "rowodd"

ns_log Notice "intranet-reporting-finance/finance-income-statement: sql=\n$sql"

db_foreach sql $sql {

    if { "" == $cost_invoices_cache } { set cost_invoices_cache 0}
    if { "" == $cost_quotes_cache } { set cost_quotes_cache 0}
 #   if { "" == $paid_amount } { set paid_amount 0}

    set cost_invoices_cache_pretty [lc_numeric $cost_invoices_cache %.${rounding_precision}f $number_locale]
    set cost_quotes_cache_pretty [lc_numeric $cost_quotes_cache %.${rounding_precision}f $number_locale]
 #   set paid_amount_pretty [lc_numeric $paid_amount %.${rounding_precision}f $number_locale]

    if {"" == $customer_id} {
	set customer_id 0
	set customer_name [lang::message::lookup "" intranet-reporting.No_customer "Undefined Customer"]

    }

        if {"" == $project_id} {
            set project_id 0
            set project_name [lang::message::lookup "" intranet-reporting.No_project "Undefined Project"]
        }

	if {"" == $project_manager_id} {
	    set project_manager_id 0
	    set project_manager_name "No Project Manager"
	}
	
	if {"" == $interco_company_id} {
	    set interco_company_id 0
	    set agency "not spec."
	}
	if {"" != $quote_name} {
             set project_name "$quote_name ($project_name)"
        }
	
    
    # Get the "interesting" company (the one that is NOT "internal")
   # set company_html "<a href=$company_url$customer_id>$customer_name</a>"
   # if {$customer_id == [im_company_internal]} {
#	set company_html "<a href=$company_url$provider_id>$provider_name</a>"
  #  }



    
    im_report_display_footer \
	-output_format $output_format \
	-group_def $report_def \
	-footer_array_list $footer_array_list \
	-last_value_array_list $last_value_list \
	-level_of_detail $level_of_detail \
	-row_class $class \
	-cell_class $class

    # Update Counters and calculate pretty counter values
    im_report_update_counters -counters $counters

  #  set paid_custsubtotal_pretty [lc_numeric $paid_custsubtotal %.${rounding_precision}f $number_locale]
 ##   set invoice_custsubtotal_pretty [lc_numeric $invoice_custsubtotal %.${rounding_precision}f $number_locale]
   
#    set paid_subtotal_pretty [lc_numeric $paid_subtotal %.${rounding_precision}f $number_locale]
    set invoice_subtotal_pretty [lc_numeric $invoice_subtotal %.${rounding_precision}f $number_locale]
   
#    set paid_total_pretty [lc_numeric $paid_total %.${rounding_precision}f $number_locale]
#    set invoice_total_pretty [lc_numeric $invoice_total %.${rounding_precision}f $number_locale]
    
    set last_value_list [im_report_render_header \
	    -output_format $output_format \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
    ]

    set footer_array_list [im_report_render_footer \
	    -output_format $output_format \
	    -group_def $report_def \
	    -last_value_array_list $last_value_list \
	    -level_of_detail $level_of_detail \
	    -row_class $class \
	    -cell_class $class
    ]
}

im_report_display_footer \
    -output_format $output_format \
    -group_def $report_def \
    -footer_array_list $footer_array_list \
    -last_value_array_list $last_value_list \
    -level_of_detail $level_of_detail \
    -display_all_footers_p 1 \
    -row_class $class \
    -cell_class $class

im_report_render_row \
    -output_format $output_format \
    -row $footer0 \
    -row_class $class \
    -cell_class $class \
    -upvar_level 1


switch $output_format {
    html { ns_write "</table>\n[im_footer]\n"}
}
