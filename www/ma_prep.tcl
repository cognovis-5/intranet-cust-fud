# packages/intranet-cust-fud/www/ma.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
	MA prep
    
    @author etm
    @creation-date 2016-11-29
    @cvs-id $Id$
} {
    invoice_id:notnull
    return_url
} 



db_dml ma_stat_upd " update im_costs set cost_status_id = 11000426 where cost_id = :invoice_id "


if { "" == $return_url } { set return_url "/intranet-cust-fud/reports/fud-paym-due_bypm2014" }
ad_returnredirect $return_url 



