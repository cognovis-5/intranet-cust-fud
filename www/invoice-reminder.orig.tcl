# /packages/intranet-cust-fud/www/invoice-reminder.tcl
#


ad_page_contract {
    Purpose: Send a reminder to all invoice recipients

    @param return_url the url to return to
    @author malte.sussdorff@cognovis.de
} {
    { return_url "/intranet-invoices/" }
    { cost:multiple "" }
    { cost_status:array "" }
    { new_payment_amount:array "" }
    { new_payment_currency:array "" }
    { new_payment_date:array "" }
    { new_payment_type_id:array "" }
    { new_payment_company_id:array "" }
    { invoice_action "" }

}

set user_id [ad_maybe_redirect_for_registration]
if {![im_permission $user_id add_invoices]} {
    ad_return_complaint 1 "<li>You have insufficient privileges to see this page"
    return
}


foreach cost_id $cost {
	
	db_1row invoice_info "select company_contact_id as recipient_id, 
						customer_id as company_id, 
						email, 
						invoice_nr, 
						effective_date, 
						now() - interval '10 days' as payment_date, 
						now() + interval '10 days' as reminder_date, 
						now()::date - effective_date::date - payment_days as overdue_days 
					  from im_invoices, parties, im_costs 
					  where party_id = company_contact_id 
						and invoice_id = :cost_id 
						and invoice_id = cost_id"
	
	# Nichts tun wenn überfällige Tage < 1
	if {$overdue_days > 0} {
	
		# Get the project information of the first linked project
		db_1row related_projects_sql "
				select distinct
			   	r.object_id_one as project_id,
				p.project_name,
				first_names || ' ' || last_name as project_lead,
				pa.email as project_lead_email,
				p.project_nr,
				p.parent_id,
				p.description,
				trim(both p.company_project_nr) as customer_project_nr,
				signature
			from
				acs_rels r,
				im_projects p,
				persons pe,
				parties pa
			where
				r.object_id_one = p.project_id
				and r.object_id_two = :cost_id
				and p.project_lead_id = pe.person_id
				and pa.party_id = pe.person_id
				limit 1
		"
		
		set recipient_locale "de_DE"	
		set effective_date_pretty [lc_time_fmt $effective_date %q $recipient_locale]
		set payment_date_pretty [lc_time_fmt $payment_date %q $recipient_locale]
		set reminder_date_pretty [lc_time_fmt $reminder_date %q $recipient_locale]
		
		# Transform the signature
		set signature [template::util::richtext::get_property html_value $signature]

		set invoice_item_id [content::item::get_id_by_name -name "${invoice_nr}.pdf" -parent_id $cost_id]
		if {"" == $invoice_item_id} {
		    set invoice_revision_id [intranet_openoffice::invoice_pdf -invoice_id $cost_id -preview_p 0]
		} else {
		    set invoice_revision_id [content::item::get_best_revision -item_id $invoice_item_id]
		}

		set subject "Zahlungserinnerung $invoice_nr"
		#set bcc "webmin@etranslationmanagement.com"
		set cc "abinsheim@yahoo.de"
		set bcc "abinsheim@gmail.com"
		
		set body "Guten Tag,<br /> <br />
bei der Überprüfung Ihres Kundenkontos hat eine Buchprüfung ergeben, dass die Zahlung für die Rechnung $invoice_nr vom $effective_date_pretty noch nicht bei uns eingegangen und seit $overdue_days Tagen überfällig ist. Hierbei wurden Zahlungseingänge bis zum $payment_date_pretty berücksichtigt.
<br /><br />Sollten Sie den fälligen Betrag inzwischen bezahlt haben, möchten wir Sie bitten, uns kurz das Datum der Zahlung mitzuteilen, da unter Umständen die Zahlung nicht zuzuordnen war. Andernfalls überweisen Sie bitte den Rechnungsbetrag innerhalb der nächsten 10 Tage, also bis zum <strong>$reminder_date_pretty</strong> auf eines der angegebenen Konten. 
<br /> Bitte geben Sie bei Rückfragen und Zahlungen immer Rechnungs- und Kundennummer an:
<br /> <strong><li>Rechnungs-Nr: $invoice_nr</strong> </li><li>Kunden-Nr: $company_id</li></strong> 
<br /> <br />Mit freundlichen Grüßen<br />$project_lead<br /><br />--<br />$signature"
		
		acs_mail_lite::send \
			-send_immediately \
			-to_addr "$email" \
			-cc_addr $cc \
			-bcc_addr $bcc \
			-from_addr "$project_lead_email" \
			-subject "$subject" \
			-body "$body" \
			-file_ids $invoice_revision_id \
			-mime_type "text/html" \
			-object_id $cost_id

		set invoice_reminder_status_id [parameter::get_from_package_key -package_key "intranet-invoices" -parameter InvoiceReminderStatusID -default ""]
		if {$invoice_reminder_status_id ne ""} {
			db_dml update_status "update im_costs set cost_status_id = :invoice_reminder_status_id where cost_id = :cost_id"
		}
		
		# Get the log for the mail just send and store the content in the cost folder.
		set log_id [db_string get_log_id "select max(log_id) from acs_mail_log where context_id = :cost_id"]
		set params [list [list log_id $log_id]]
		set message_html [ad_text_to_html -includes_html -encode -no_quote [ad_parse_template -params $params /packages/intranet-mail/lib/one-message]]
		
		set cost_path [im_filestorage_cost_path $cost_id]
		if {![file exists $cost_path]} {
			file mkdir $cost_path
		}
		set file [open "${cost_path}/reminder_${log_id}.html" w]
		fconfigure $file -encoding "utf-8"
		puts $file $message_html
		flush $file
		close $file

	}
}

ad_returnredirect $return_url


