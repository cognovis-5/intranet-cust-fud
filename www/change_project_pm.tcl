# packages/intranet-cust-fud/www/cost_status.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
	change project lead
    
    @author etm
    @creation-date 2017-05-29
    @cvs-id $Id$
} {
    
    cm_id:notnull
    project_id:notnull
    return_url
} 


set cmdept_id [db_string dept "select department_id as cmdept_id from im_employees where employee_id = :cm_id " -default ""]

db_dml change_pm_upd "update im_projects set project_lead_id = :cm_id where project_id = :project_id and project_cost_center_id = :cmdept_id"


if { "" == $return_url } { set return_url "/intranet/projects/view?project_id=$project_id" }
ad_returnredirect $return_url 



