# packages/intranet-cust-fud/www/cost_status.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
	coststatus change
    
    @author etm
    @creation-date 2017-09-29
    @cvs-id $Id$
} {
    
    
    company_id:notnull
    ft:optional
    company_status_id:optional
    return_url:optional
} 

if { "" != $ft } {
	if { $ft == "t" } { set fORt "t" 
	} elseif { $ft == "f" } { set fORt "f" 
	} else   { ad_return_complaint 1 "bitte admin benachrichtigen: falscher Parameter fuer noautoreminder" } 

	db_dml upd "update im_companies set noautoreminder = '$fORt' where company_id =  :company_id"
}

if { "" != $company_status_id } {
	 set category_type "Intranet Company Status"
	 db_string company_status "select category_id, category as company_status from im_categories where category = :company_status_id and category_type = :category_type" -default ""
	
	  if {"" == $company_status_id} { 
		ad_return_complaint 1 "<b>Internal Error</b>:<br>im_category_is_a: Category '$company_status' is not part of '$category_type'" 
		ad_script_abort
		} else {
				db_dml upd "update im_companies set company_status_id = '$company_status_id' where company_id =  :company_id"
				}
}
				
if { "" == $return_url } { set return_url "/intranet-core/companies/view?company_id=$company_id" }
ad_returnredirect $return_url 



