SELECT acs_log__debug('/packages/intranet-cust-fud/sql/postgresql/upgrade/upgrade-4.1.0.0.0-5.1.0.0.0.sql','');

create or replace view users_active as
select
        u.user_id, u.username, u.screen_name, u.last_visit, u.second_to_last_visit, u.n_sessions, u.first_names, u.last_name,
        c.home_phone, c.priv_home_phone, c.work_phone, c.priv_work_phone, c.cell_phone, c.priv_cell_phone, c.pager, c.priv_pager,
        c.fax, c.priv_fax, c.aim_screen_name, c.priv_aim_screen_name, c.msn_screen_name, c.priv_msn_screen_name, c.icq_number,
        c.priv_icq_number, c.m_address, c.ha_line1, c.ha_line2, c.ha_city, c.ha_state, c.ha_postal_code, c.ha_country_code,
        c.priv_ha, c.wa_line1, c.wa_line2, c.wa_city, c.wa_state, c.wa_postal_code, c.wa_country_code, c.priv_wa,
        c.note, c.current_information
from    registered_users u left outer join users_contact c on u.user_id = c.user_id;

create or replace view im_employees_active as
select
        u.*,
        e.*,
        pa.*,
        pe.*
from
        users u,
        group_distinct_member_map gdmm,
        parties pa,
        persons pe
        LEFT OUTER JOIN im_employees e ON (pe.person_id = e.employee_id)
where
        u.user_id = pa.party_id and
        u.user_id = pe.person_id and
        u.user_id = e.employee_id and
        u.user_id = gdmm.member_id and
        gdmm.group_id in (select group_id from groups where group_name = 'Employees') and
        u.user_id in (
                select  r.object_id_two
                from    acs_rels r,
                        membership_rels mr
                where   r.rel_id = mr.rel_id and
                        r.object_id_one in (
                                select group_id
                                from groups
                                where group_name = 'Registered Users'
                        ) and mr.member_state = 'approved'
        )
;

update im_component_plugins set component_tcl = 'cog::project::active_projects_component -project_status_id "[im_project_status_open],[im_project_status_potential]"' where plugin_id = 2740392;
update im_component_plugins set component_tcl = 'cog::project::active_projects_component -project_lead_id [ad_conn user_id] -project_status_id "[im_project_status_inquiring]"', title_tcl = 'lang::message::lookup "" cognovis-core.Inquiry_Projects "Inquiry Projects"' where plugin_id = 11379;