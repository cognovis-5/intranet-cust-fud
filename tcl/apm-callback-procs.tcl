
ad_library {
    APM callback procedures.

    @creation-date 2021-08-06
    @author malte.sussdorff@cognovis.de
}

namespace eval fud::apm {}

ad_proc -public fud::apm::after_upgrade {
    {-from_version_name:required}
    {-to_version_name:required}
} {
    After upgrade callback.
} {
    apm_upgrade_logic \
        -from_version_name $from_version_name \
        -to_version_name $to_version_name \
        -spec {
            4.1.0.0.0 5.1.0.0.0 {
                if {[apm_package_installed_p "sencha-invoices"]} {
                    apm_package_delete sencha-invoices
                }
                if {[apm_package_installed_p "sencha-dynamic"]} {
                    apm_package_delete sencha-dynamic
                }
                if {[apm_package_installed_p "intranet-reporting-finance"]} {
                    apm_package_delete intranet-reporting-finance
                }
                if {[apm_package_installed_p "intranet-update-client"]} {
                    apm_package_delete intranet-update-client
                }
                if {[apm_package_installed_p "intranet-slack"]} {
                    apm_package_delete intranet-slack
                }
                if {[apm_package_installed_p "bug-tracker"]} {
                    apm_package_delete bug-tracker
                }
                if {[apm_package_installed_p "xml-rpc"]} {
                    apm_package_delete xml-rpc
                }
                if {[apm_package_installed_p "wiki"]} {
                    apm_package_delete wiki
                }
            }
        }
}

ad_proc -public fud::apm::after_install {} {
    Setup cognovis core
} {

    # Remove richtext from notes.
    # Make it all html

    fud::apm::after_upgrade -from_version_name 4.0.0.0.0 -to_version_name 5.3.0.0.0

}
